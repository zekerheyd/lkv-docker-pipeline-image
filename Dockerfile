FROM php:7.4-fpm

RUN mkdir /code

WORKDIR /code

# Install nodejs 14
RUN apt-get update && \
    apt-get install -y sudo curl gcc g++ make && \
    curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -  && \
    apt-get install -y nodejs

RUN echo "NODE Version:" && node --version
RUN echo "NPM Version:" && npm --version

RUN apt-get update && \
    apt-get install -y ruby && \
    apt-get install -y unzip zlib1g-dev libzip-dev libxml2-dev libmagickwand-dev default-mysql-client && \ 
    apt-get install -y git gnupg2 libmcrypt-dev --no-install-recommends && \
    apt-get install -y ghostscript-x tesseract-ocr tesseract-ocr-nld wget curl xvfb chromium  && \
    docker-php-ext-install zip && \
    docker-php-ext-install bcmath && \
    docker-php-ext-install exif && \
    docker-php-ext-install pdo_mysql && \
    docker-php-ext-install gd && \
    docker-php-ext-install pcntl && \
    docker-php-ext-install soap && \
    pecl install imagick && \
    docker-php-ext-enable zip && \
    docker-php-ext-enable imagick && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    apt-get update && apt-get upgrade -y && apt-get install -y && \
    apt-get purge --auto-remove -y curl gnupg && \
    rm -rf /var/lib/apt/lists/*

# Enable PDF to PNG conversion with GhostScript
RUN sed -i "s/none\"\ pattern=\"PDF\"/read\|write\"\ pattern=\"PDF\"/g" /etc/ImageMagick-6/policy.xml